/* 
 * Copyright (C) Origma Pty Ltd (ACN 629 381 184). All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ben McLean <ben@origma.com.au>, November 2019
 */
package au.com.origma.metadata;

import java.util.HashMap;
import java.util.Map;

public class MetadataCollection {

	Map<String, FieldMetadata> fieldMetadata;
	
	public MetadataCollection(){
		fieldMetadata = new HashMap<>();
	}

	public Map<String, FieldMetadata> getFieldMetadata() {
		return fieldMetadata;
	}

	public void setFieldMetadata(Map<String, FieldMetadata> fieldMetadata) {
		this.fieldMetadata = fieldMetadata;
	}
	
}
